# Client.py
# File for sending requests to the server

__author__ = 'ashwin'
import socket
import json


clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientSocket.connect(('127.0.0.1', 10000))

# Get the number of file operations that the user is going to perform
numOperations = raw_input("Enter the Number of file operations:")

# Create a task list for sending data to the server
taskList = []

# For each operation, perform the below operations
for i in range(0, int(numOperations)):
    print "\nOperation Number: "+str(i+1)
    print "-----------------------------\n"
    file_name = raw_input("Enter name of the file: ")
    print "\nFollowing operations are permissible:\n"
    print " 1. Read the file\n 2. Delete the file\n 3. Delete line numbers in file"
    print " 4. Delete specific text lines in file"
    print " 5. Update specific text lines in file\n"
    data = {};
    option = int(raw_input("Enter your option: "));

    if option == 1:
        print "Reading file"
        taskList.append({"READ":file_name})

    elif option == 2:
        print "Delete file"
        taskList.append({"DELETE":file_name})

    elif option == 3:
        print "Delete line numbers from file"
        del_line = raw_input("Enter the line number to the deleted: ")
        taskList.append({"DELETE_TEXT_LINE_NUMBERS":[{"FILE_NAME":file_name, "DEL_LINE":del_line}]})

    elif option == 4:
        print "Delete specific text lines from file"
        del_text = raw_input("Enter the text to the deleted: ")
        taskList.append({"DELETE_TEXT_LINES":[{"FILE_NAME":file_name, "DEL_TEXT":del_text}]})

    elif option == 5:
        print "Update specific text lines from file"
        update_text_old= raw_input("Enter the OLD text to the updated (Text Replacement!) : ")
        update_text_new= raw_input("Enter the NEW text to the updated (Text Replacement!) : ")
        taskList.append({"UPDATE_TEXT_LINES":[{"FILE_NAME":file_name, "UPDATE_TEXT_OLD":update_text_old
                                              , "UPDATE_TEXT_NEW":update_text_new}]})



# Send the task list request to the server
clientSocket.send(json.dumps({'TASKS':taskList}))

# Get the JSON result from the server, and start displaying the outputs
result = json.loads(clientSocket.recv(1024))
if 'READ_RESPONSE' in result:
    print result['READ_RESPONSE']

if 'READ_ERROR' in result:
    print result['READ_ERROR']

if 'DELETE_RESPONSE' in result:
    print result['DELETE_RESPONSE']

if 'DELETE_ERROR' in result:
    print result['DELETE_ERROR']

if 'DELETE_TEXT_LINE_NUMBERS_RESPONSE' in result:
    print result['DELETE_TEXT_LINE_NUMBERS_RESPONSE']

if 'DELETE_TEXT_LINE_NUMBERS_RESPONSE_ERROR' in result:
    print result['DELETE_TEXT_LINE_NUMBERS_RESPONSE_ERROR']

if 'DELETE_TEXT_LINES_RESPONSE' in result:
    print result['DELETE_TEXT_LINES_RESPONSE']

if 'DELETE_TEXT_LINES_RESPONSE_ERROR' in result:
    print result['DELETE_TEXT_LINES_RESPONSE_ERROR']

if 'UPDATE_TEXT_LINES_RESPONSE' in result:
    print result['UPDATE_TEXT_LINES_RESPONSE']

if 'UPDATE_TEXT_LINES_RESPONSE_ERROR' in result:
    print result['UPDATE_TEXT_LINES_RESPONSE_ERROR']

# Close the current client session
clientSocket.close()
