# Server.py
# Server file for accepting client requests for file read and modifications
__author__ = 'ashwin'
import SocketServer
import json
import os

class Server(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True

class ServerHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = {}
        sendingList = []
        try:
            # Get the JSON data that the client has sent
            data = json.loads(self.request.recv(1024).strip())
            print "Received following task list: "
            print data
            # Get the list of tasks that client sends
            tasks = data['TASKS']

            # Perform file operations for each of the task as requested by the client
            for k in tasks:

                # Operations to be done for each Read Task
                if 'READ' in k:
                    try:
                        inputFile = open(k['READ'],"r")
                        file_contents = inputFile.read()
                        data['READ_RESPONSE'] = "Contents of "+k['READ']+" are: \n"+file_contents
                    except IOError:
                       data['READ_ERROR'] = "Requested File "+ k['READ']+" for READING Not Found"

                # Operations to be done for each Delete File Task
                elif 'DELETE' in k:
                    try:
                        os.remove(k['DELETE'])
                        data['DELETE_RESPONSE'] = "Deleted "+k['DELETE']
                    except OSError:
                        data['DELETE_ERROR'] = "Requested File "+k['DELETE']+" for DELETING Not Found"

                # Operations to be done for Deleting specific line numbers
                elif 'DELETE_TEXT_LINE_NUMBERS' in k:
                    delFileName = ""
                    delFileLine = ""

                    subtasks = k['DELETE_TEXT_LINE_NUMBERS']
                    for j in subtasks:
                        if 'FILE_NAME' in j:
                            delFileName = j['FILE_NAME']
                            delFileLine = j['DEL_LINE']

                    try:
                        fileForDelLine = open(delFileName,"r")
                        fileLinesRead = fileForDelLine.readlines()
                        fileForDelLine.close()

                        fileForDelLine = open(delFileName,"w")
                        lineCounter = 1
                        for line in fileLinesRead:
                            if (lineCounter == int(delFileLine)):
                                print "Deleted Required Line"
                            else:
                                fileForDelLine.write(line)
                            lineCounter += 1
                        fileForDelLine.close()
                        data['DELETE_TEXT_LINE_NUMBERS_RESPONSE'] = "Completed deleting line "+\
                                                                    delFileLine+" from "+delFileName
                    except IOError:
                        data['DELETE_TEXT_LINE_NUMBERS_RESPONSE_ERROR'] = "Requested File "+\
                                                                          delFileName+" for DELETING LINES Not Found"

                # Operations to be done for Deleting lines containing certain text
                elif 'DELETE_TEXT_LINES' in k:
                    delFileName = ""
                    delFileText = ""

                    subtasks = k['DELETE_TEXT_LINES']
                    for j in subtasks:
                        if 'FILE_NAME' in j:
                            delFileName = j['FILE_NAME']
                            delFileText = j['DEL_TEXT']
                    try:
                        fileForDel = open(delFileName,"r")
                        fileLines = fileForDel.readlines()
                        fileForDel.close()
                        flag = 0
                        fileForDel = open(delFileName,"w")
                        for line in fileLines:
                            if ( not (line.__contains__(delFileText))):
                                fileForDel.write(line)
                            else:
                                flag = 1
                        fileForDel.close()
                        if flag == 1:
                            data['DELETE_TEXT_LINES_RESPONSE'] = "Completed deleting line containing "+\
                                                                    delFileText+" from "+delFileName
                        else:
                            data['DELETE_TEXT_LINES_RESPONSE'] = "No line containing "+\
                                                                    delFileText+" found in "+delFileName
                    except IOError:
                        data['DELETE_TEXT_LINES_RESPONSE_ERROR'] = "Requested File for "+ delFileName+" DELETING TEXT LINES Not Found"

                # Update: Text Replacement
                elif 'UPDATE_TEXT_LINES' in k:

                    subtasks = k['UPDATE_TEXT_LINES']
                    for j in subtasks:
                        if 'FILE_NAME' in j:
                            updateFileName = j['FILE_NAME']
                            updateFileTextOld = j['UPDATE_TEXT_OLD']
                            updateFileTextNew = j['UPDATE_TEXT_NEW']

                    try:
                        fileForUpdate = open(updateFileName,"r")
                        fileLines = fileForUpdate.readlines()
                        fileForUpdate.close()

                        fileForUpdate = open(updateFileName,"w")
                        updateFlag = 0
                        for line in fileLines:
                            if line.__contains__(updateFileTextOld):
                                fileForUpdate.write(line.replace(updateFileTextOld, updateFileTextNew))
                                updateFlag = 1
                            else:
                                fileForUpdate.write(line)
                        fileForUpdate.close()
                        if updateFlag == 1:
                            data['UPDATE_TEXT_LINES_RESPONSE'] = "Text Replacement Update Complete for "+updateFileName
                        else:
                            data['UPDATE_TEXT_LINES_RESPONSE'] = "No line containing " +\
                                                                 updateFileTextOld+" found in "+updateFileName
                    except IOError:
                        data['UPDATE_TEXT_LINES_RESPONSE_ERROR'] = "Requested File "+ updateFileName +" for UPDATING Not Found"

            self.request.sendall(json.dumps(data))

        except Exception, e:
            print "Error Message: ", e


# Start the Server and listen for clients requests in a loop
server = Server(('127.0.0.1', 10000), ServerHandler)
print "Server Started"
server.serve_forever()
