# README #

This application is developed as a submission for the Automation QA Internship at GapJumpers.


### Client Server File Operations using JSON ###

* A Python application that allows file operations to be performed by clients on the files that are resident on the server. The communication format between the client and the server is JSON.
* Version 1.0

### Important Alternate Link ###
During submission, I had thought that the repository was supposed to be made private, so I used bitbucket for it. But later was realized that the repository needs to be public. So, I have created a same replica of this repository on GitHub as well. Please find the link here: [GitHub Link](https://github.com/ashwintumma23/gapJumperClientServerFileOperations.git) 

### Who do I talk to? ###

* Ashwin Tumma 
* Email: <ashwin.tumma@stonybrook.edu>
* [Website](https://sites.google.com/site/ashwintumma23/)
* State University of New York, Stony Brook

### License ###
* GNU GPL v2 - Check the LICENSE file for the license

### Implementation Plan and Algorithm ###
This section elaborates upon the Implementation and the Algorithm that was followed for the development of this application.

#### Implementation
The implementation of the application is done using Python 2.7.6. [pyCharm](https://www.jetbrains.com/pycharm/) IDE was used for development. Testing is also done by running the application on the terminal

#### Algorithm
The application is comprised of two distinct modules, the **Server** and the **Client**. The server is a process which accepts the HTTP requests, and has the data files saved with it. Client process sends the server process HTTP requests for reading, deleting and updating files. The communication is done in JSON format. 

Essentially, the end user will run the two process and the client will send HTTP requests to the server. The server reads the requests (JSON objects), parses the same to see what requests are to be served, and processes them using the native server machine commands.

### Source Files
All the source files are in the `src` directory. Here's a brief description of each one of them: 

* Server.py: Source code for Server
* Client.py: Source code for Client
* A.txt: Sample file at the server
* B.txt: Sample file at the server

### How to run the Application? ###

#### Server
Start the server process by running `python Server.py` from the terminal

#### Client
Start the server process by running `python Client.py` from the terminal. Following are examples of few usage scenarios of the application

##### Reading the file from the server
```
#!python

Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: A.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 1
Reading file
Contents of A.txt are: 
Ashwin Tumma
Computer Science Graduate Student
Stony Brook University, New York
```
##### Deleting a non existing file from the server
```
Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: Ashwin.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 2
Delete file
Requested File Ashwin.txt for DELETING Not Found
```

##### Deleting an existing file from the server
```
Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: A.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 2
Delete file
Deleted A.txt
```
##### Deleting a specific line from a file
```
Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: A.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 3
Delete line numbers from file
Enter the line number to the deleted: 1
Completed deleting line 1 from A.txt
```
##### Deleting a line containing certain text which is not in a file
```
Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: A.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 4
Delete specific text lines from file
Enter the text to the deleted: TESTING
No line containing TESTING found in A.txt
```
##### Deleting a line containing certain text which is there in a file
```
Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: A.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 4
Delete specific text lines from file
Enter the text to the deleted: Computer
Completed deleting line containing Computer from A.txt
```
##### Updating a line containing a certain string which is not there in the file
```
Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: A.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 5
Update specific text lines from file
Enter the OLD text to the updated (Text Replacement!) : San Francisco 
Enter the NEW text to the updated (Text Replacement!) : San Jose
No line containing San Francisco found in A.txt
```
##### Updating a line containing a certain string which is there in the file
```
Enter the Number of file operations:1

Operation Number: 1
-----------------------------

Enter name of the file: A.txt

Following operations are permissible:

 1. Read the file
 2. Delete the file
 3. Delete line numbers in file
 4. Delete specific text lines in file
 5. Update specific text lines in file

Enter your option: 5
Update specific text lines from file
Enter the OLD text to the updated (Text Replacement!) : New York
Enter the NEW text to the updated (Text Replacement!) : San Diego
Text Replacement Update Complete for A.txt
```

### Future Enhancements ###
* Currently at the client side, a single response of the same type is being processed, multiple responses code needs to be added
* Development of a Graphical user interface for the client application. This can be easily done using GTK toolkit for Python
* Currently, the application is running on localhost. A minor change with a command line argument which states on which IP address the server is running can be done to scale out the application on the Internet.


### Additional Details ###
* Dependencies: 
Python 2.7.6

* Database configuration: 
Not Applicable

